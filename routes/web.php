<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('lang/{language}', ['as' => 'lang.switch', 'uses' => 'LanguageController@switchLang']);

Route::get('/', 'Website\FrontController@index');
Route::get('/home', 'Website\FrontController@index');
Route::get('/product/{slug}', ['as' => 'product.show', 'uses' => 'Website\SingleController@show']);

Route::get('/order/{slug}', ['as' => 'product.order', 'uses' => 'Website\SingleController@order']);

//Show Join Credentials
Route::get('/login', 'UserAuth\LoginController@loginView')->name('login');
Route::get('/register', 'UserAuth\RegisterController@show')->name('register');

//Auth:register
Route::post('/register', 'UserAuth\RegisterController@register');

//Auth:login
Route::post('/login', 'UserAuth\LoginController@login');

//Auth logout
Route::post('/logout', 'UserAuth\LoginController@logout');

Route::group(['middleware' => 'auth'], function () {
  
  /**
   * @description CRUD of (Product Model) for Auth User.
   * @index Show all products.
   * @create Show the view to create a product.
   * @store Save product in database.
   * @edit Show the view to edit product info.
   * @update Update product data in database.
   * @destroy Delete a product from database.
  */
  Route::get('/my-products', 'UserAuth\ProductController@index');
  Route::get('/add-product', 'UserAuth\ProductController@create');
  Route::post('/add-product', 'UserAuth\ProductController@store');
  Route::get('/edit-product/{id}', ['as' => 'product.edit', 'uses' => 'UserAuth\ProductController@edit']);
  Route::patch('/edit-product/{id}', 'UserAuth\ProductController@update');
  Route::post('/delete-product/{id}', 'UserAuth\ProductController@destroy');

});


Route::get('/admin/login', 'AdminAuth\LoginController@showLoginForm')->name('login');
Route::post('/admin/login', 'AdminAuth\LoginController@login');

Route::group(['prefix' => 'admin', 'middleware' => 'admin'], function () {
  
  Route::get('/home', function () {
    return view('admin.home');
  });

  Route::post('/logout', 'AdminAuth\LoginController@logout')->name('logout');

  /**
   * @index Show all admins.
   * @create Show the view to create admin.
   * @store Save admin in database.
   * @edit Show the view to edit admin info.
   * @update Update admin data in database.
   * @destroy Delete a admin from database.
 */
  Route::get('/admins', 'Admin\AdminController@index');
  Route::get('/edit-admin/{id}', 'Admin\AdminController@edit');
  Route::patch('/edit-admin/{id}', 'Admin\AdminController@update');
  Route::post('/delete-admin/{id}', 'Admin\AdminController@destroy');
  Route::get('/add-admin', 'Admin\AdminController@create');
  Route::post('/add-admin', 'Admin\AdminController@store');

  /**
   * @description CRUD of (User Model)
   * @index Show all Users.
   * @create Show the view to create a user.
   * @store Save user in database.
   * @edit Show the view to edit user info.
   * @update Update user data in database.
   * @destroy Delete a user from database.
  */
  Route::get('/users', 'Admin\UserController@index');
  Route::get('/add-user', 'Admin\UserController@create');
  Route::post('/add-user', 'Admin\UserController@store');
  Route::get('/edit-user/{id}', 'Admin\UserController@edit');
  Route::patch('/edit-user/{id}', 'Admin\UserController@update');
  Route::post('/delete-user/{id}', 'Admin\UserController@destroy');

  /**
   * @description CRUD of (Product Model).
   * @index Show all products.
   * @create Show the view to create a product.
   * @store Save product in database.
   * @edit Show the view to edit product info.
   * @update Update product data in database.
   * @destroy Delete a product from database.
  */
  Route::get('/products', 'Admin\ProductController@index');
  Route::get('/add-product', 'Admin\ProductController@create');
  Route::post('/add-product', 'Admin\ProductController@store');
  Route::get('/edit-product/{id}', 'Admin\ProductController@edit');
  Route::patch('/edit-product/{id}', 'Admin\ProductController@update');
  Route::post('/delete-product/{id}', 'Admin\ProductController@destroy');

  /**
   * @description All Orders of (Order Model), Delete & Approve Order.
   * @approve Approve Order to ordered user.
   * @unapprove Unapprove Order to ordered user.
   * @destroy Delete a Order from database.
  */
  Route::get('/orders', 'Admin\OrderController@index');
  Route::post('/delete-order/{id}', 'Admin\OrderController@destroy');
  Route::post('/approve-order/{id}', 'Admin\OrderController@approve');
  Route::post('/unapprove-order/{id}', 'Admin\OrderController@unapprove');

});
