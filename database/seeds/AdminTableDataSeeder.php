<?php

use Illuminate\Database\Seeder;
use App\Admin;

class AdminTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Admin::create([
                'name' => 'Developer Access',
                'email' => 'reda19952013@gmail.com',
                'password' => bcrypt('123456'),
                'last_login' => now(),
                'remember_token' => 'ggItpjiDtNAJ9TmNGTWlh5B5nQX9VYU79yHaBRggDLj3LJ9TIuExpEcSDRUo',
                'created_at' => '2019-05-28 17:03:28',
                'updated_at' => '2019-05-28 17:05:00',
            ]); 
    }
}
