@extends('admin.master')
@section('content')

<form class="form-horizontal" role="form" method="POST" action="/{{ Request::segment(1) }}/admin/add-admin">
@csrf
  <div class="box box-info">
      <div class="box-header with-border">
          <h4 class="card-title">Add new Admin</h4>
            <p class="card-description">
              Accounts for admins
            </p>
      </div>
      <!-- /.box-header -->
      <!-- form start -->
      <form class="form-horizontal">
        <div class="box-body">
          <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">Name *</label>

            <div class="col-sm-10">
              <input type="text" name="name" class="form-control" id="inputEmail3" placeholder="Name">
            @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
            @endif
            </div>
          </div>
          <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">Email *</label>

            <div class="col-sm-10">
              <input type="email" class="form-control" id="inputEmail3" placeholder="Email" name="email">
              @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
            @endif
            </div>
            
          </div>
          <div class="form-group">
            <label for="inputPassword3" class="col-sm-2 control-label">Password *</label>

            <div class="col-sm-10">
              <input type="password" class="form-control" id="inputPassword3" placeholder="Password" name="password">
              @if ($errors->has('password'))
            <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
            @endif
            </div>
            
          </div>

          <div class="form-group">
            <label for="inputPassword3" class="col-sm-2 control-label">Confirm Password *</label>

            <div class="col-sm-10">
              <input type="password" class="form-control" id="inputPassword3" placeholder="Confirm Password" name="password_confirmation">
               @if ($errors->has('password_confirmation'))
            <span class="help-block">
                <strong>{{ $errors->first('password_confirmation') }}</strong>
            </span>
            @endif
            </div>
           
          </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          <a href="{{ url()->previous() }}"><button class="btn btn-default">Cancel</button></a>
          <button type="submit" class="btn btn-info pull-right">Save</button>
        </div>
        <!-- /.box-footer -->
      </form>
    </div>
</form>
@endsection
