@extends('admin.master')
@section('content')
@if($flash = session('message'))
<div class="alert success">
  <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
    <b>{{ $flash }}</b>
</div>
@endif
@if($flash = session('deleted'))
<div class="alert">
  <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
    <b>{{ $flash }}</b>
</div>
@endif

<div class="box">
            <div class="box-header">
  <h3 class="box-title">Data Table For Products</h3>
</div>
    <div class="box-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
          <th>ID</th>
          <th>Product Owners</th>
          <th>Product Title</th>
          <th>Product Content</th>
          <th>Product Price</th>
          <th>Product Image</th>
          <th>Posted at</th>
          <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($products as $product)
        <tr>
          <td>{{ $product->id }}</td>
          <td>{{ $product->owner->name }}</td>
          <td>
            <ul>
              @foreach(config('translatable.locales') as $l => $lan)
              <b>{{ $lan }}</b>
                <li>{{ $product->translate($l)->title }}</li>
              @endforeach
            </ul>
          </td>
          <td>
            <ul>
               @foreach(config('translatable.locales') as $lo => $lang)
              <b>{{ $lang }}</b>
                <li>{!! $product->translate($lo)->content !!}</li>
              @endforeach
            </ul>
          </td>
          <td>{{ $product->price }}</td>
          <td><img src="/uploads/{{ $product->image }}" width="175" class="img-responsive"></td>
          <td>{{ $product->created_at->toFormattedDateString() }}</td>
          <td>
             <a href="/{{ Request::segment(1) }}/admin/edit-product/{{ $product->id }}" class="btn btn-app" style="background-color: #5ac695">
                 <i class="fa fa-edit"></i> Edit
              </a>
              <form action="/{{ Request::segment(1) }}/admin/delete-product/{{ $product->id }}" method="POST" style="display:inline!important">
              @csrf
              <button type="submit" onclick="if (!confirm('Are you sure you want to delete?')) { return false }" class="btn btn-app delete" style="background-color: #dd4b39; color: white">
                   <i class="fa fa-trash"></i> Delete
              </button>
            </form>
          </td>
        </tr>
        @endforeach
        </tbody>
      </table>
    </div>
  </div>
@endsection