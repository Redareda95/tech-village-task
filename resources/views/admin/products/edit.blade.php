@extends('admin.master')
@section('content')

<form method="POST" action="/{{ Request::segment(1) }}/admin/edit-product/{{ $product->id }}"  enctype="multipart/form-data">
@csrf
<input type="hidden" name="_method" value="PATCH">

<div class="box box-danger">
  <div class="box-header with-border">
    <h3 class="box-title">Edit Product</h3>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-4">
      <label for="exampleInputPAN6" class="col-sm-5 col-form-label">Price *</label>

        <input type="number" class="form-control" id="exampleInputPAN6" value="{{ $product->price }}" name="price">

            @if ($errors->has('price'))
            <span class="help-block">
                <strong>{{ $errors->first('price') }}</strong>
            </span>
            @endif
      </div>
      <div class="col-md-4">
        <label for="exampleSelectSuccess">User Name *</label>
        <select class="form-control border-success chosen" id="exampleSelectSuccess" name="user_id">
          @foreach($users as $user)
          <option value="{{ $user->id }}" {{ $user->id == $product->user_id ? 'selected' : '' }}>{{ $user->name }}</option>
          @endforeach
        </select>
        @if ($errors->has('user_id'))
        <span class="help-block">
            <strong>{{ $errors->first('user_id') }}</strong>
        </span>
        @endif
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label>Upload Image <code>Maximum Size 2MB</code> *</label>
            <input type="file" name="image" class="form-control" id="imgInp">
            <h5>Old Photo</h5>
            <img id="blah" src="/uploads/{{ $product->image }}" class="img-responsive" width="300" />

              @if ($errors->has('image'))
              <span class="help-block">
                <strong>{{ $errors->first('image') }}</strong>
              </span>
              @endif
        </div>
      </div>
    </div>

    <div class="row">
      @foreach (config('translatable.locales') as $lang => $language)

      <div class="col-md-12">
        <div class="form-group">
          <label for="exampleInputEmail1">Product Title * <code>In {{ $language }}</code>
          </label>
          <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter Product Title in {{ $language }}" name="title:{{ $lang }}" value="{{ $product->translate($lang)->title }}">
           @if ($errors->has("{{ 'title:' .  $lang }}"))
            <span class="help-block">
                <strong>{{ $errors->first('title: . $lang') }}</strong>
            </span>
            @endif
        </div>

        <hr>
        <div class="box box-danger">
          <div class="box-header">
            <h3 class="box-title">Product Content * <code>In {{ $language }}</code>
            </h3>
            <div class="pull-right box-tools">
              <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip"
                      title="Collapse">
                <i class="fa fa-minus"></i></button>
              <button type="button" class="btn btn-danger btn-sm" data-widget="remove" data-toggle="tooltip"
                      title="Remove">
                <i class="fa fa-times"></i></button>
            </div>
          </div>
          <div class="box-body pad">
            <textarea id="editor_{{ $lang }}" name="content:{{ $lang }}" rows="10" cols="80">{!! $product->translate($lang)->content !!}</textarea>
            @if ($errors->has("{{ 'content:' . $lang }}"))
            <span class="help-block">
                <strong>{{ $errors->first('content: . $lang') }}</strong>
            </span>
            @endif
          </div>
        </div>
      </div>
      @endforeach
    </div>
  </div>
<div class="box-footer">
<button type="submit" class="btn btn-success btn-block btn-flat">Save</button>
</div>
</form>

@endsection