  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="/dist/img/avatar.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{ Auth::guard('admin')->user()->name }}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
         
        <li class="{{ Request::segment(2) === 'home' ? 'active' : null }}">
          <a href="/admin/home"><i class="fa fa-dashboard"></i><span> Dashboard</span></a>
        </li>

        <li class="header">Registered Users</li>

        <li class="treeview {{ Request::segment(3) === 'users' || Request::segment(3) === 'add-user' || Request::segment(3) === 'edit-user'? 'active' : null }}">
          <a href="#">
            <i class="fa fa-sliders"></i>
            <span>Users</span>
            <span class="pull-right-container">
              <span class="label label-primary pull-right">{{ \App\User::count() }}</span>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="{{ Request::segment(3) === 'users' ? 'active' : null }}"><a href="/{{ Request::segment(1) }}/admin/users"><i class="fa fa-circle-o text-yellow"></i> All Users</a></li>
            <li class="{{ Request::segment(3) === 'add-user' ? 'active' : null }}"><a href="/{{ Request::segment(1) }}/admin/add-user"><i class="fa fa-circle-o text-red"></i> Add User</a></li>
          </ul>
        </li>

        <li class="header">Products &amp; Orders</li>

        <li class="treeview {{ Request::segment(3) === 'products' || Request::segment(3) === 'add-product' || Request::segment(3) === 'edit-product'? 'active' : null }}">
          <a href="#">
            <i class="fa fa-building"></i>
            <span>Products</span>
            <span class="pull-right-container">
              <span class="label label-primary pull-right">{{ \App\Product::count() }}</span>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="{{ Request::segment(3) === 'products' ? 'active' : null }}">
              <a href="/{{ Request::segment(1) }}/admin/products"><i class="fa fa-circle-o"></i> All Products</a>
            </li>
            <li class="{{ Request::segment(3) === 'add-product' ? 'active' : null }}">
              <a href="/{{ Request::segment(1) }}/admin/add-product"><i class="fa fa-circle-o"></i> Add Product</a>
            </li>
          </ul>
        </li>
         
         <li class="treeview {{ Request::segment(3) === 'facilities' || Request::segment(3) === 'add-facility' || Request::segment(3) === 'edit-facility'? 'active' : null }}">
          <a href="#">
            <i class="fa fa-expand"></i>
            <span>Orders</span>
            <span class="pull-right-container">
              <span class="label label-primary pull-right">{{ \App\Order::count() }}</span>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="{{ Request::segment(3) === 'orders' ? 'active' : null }}">
              <a href="/{{ Request::segment(1) }}/admin/orders"><i class="fa fa-circle-o"></i> All Orders</a>
            </li>
          </ul>
        </li>

        <li class="header">Admins</li>

        <li class="treeview {{ Request::segment(3) === 'admins' || Request::segment(3) === 'add-admin'? 'active' : null }}">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Admins</span>
            <span class="pull-right-container">
              <span class="label label-primary pull-right">{{ \App\Admin::count() }}</span>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="{{ Request::segment(3) === 'admins' ? 'active' : null }}">
              <a href="/{{ Request::segment(1) }}/admin/admins"><i class="fa fa-circle-o text-yellow"></i> All Admins</a>
            </li>
            <li class="{{ Request::segment(3) === 'add-admin' ? 'active' : null }}">
              <a href="/{{ Request::segment(1) }}/admin/add-admin"><i class="fa fa-circle-o text-red"></i> Add Admin</a>
            </li>
          </ul>
        </li>

        <li class="header">Other Options</li>

        <li>
          <a href="/" target="_blank">
            <i class="fa fa-eye text-yellow"></i> 
            <span>View Site</span>
          </a>
        </li>
        
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
  <div class="content-wrapper">

