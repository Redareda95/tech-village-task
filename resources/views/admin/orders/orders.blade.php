@extends('admin.master')
@section('content')
@if($flash = session('message'))
<div class="alert success">
  <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
    <b>{{ $flash }}</b>
</div>
@endif
@if($flash = session('deleted'))
<div class="alert">
  <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
    <b>{{ $flash }}</b>
</div>
@endif

<div class="box">
            <div class="box-header">
  <h3 class="box-title">Data Table For All Orders</h3>
</div>
    <div class="box-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
          <th>ID</th>
          <th>Order User</th>
          <th>Ordered Product.</th>
          <th>Ordered Product Owner</th>
          <th>Ordered at</th>
          <th>Actions</th>
        </tr>
        </thead>
        <tbody>
      @foreach($orders as $order)
        <tr>
          <td>{{ $order->id }}</td>
          <td>{{ $order->user->name }}</td>
          <td>{{ $order->product->translate(App::getLocale())->title }}</td>
          <td>{{ $order->product->owner->name }}</td>
          <td><button class="btn btn-info" disabled>{{ $order->status }}</button></td>
          <td>{{ $order->created_at->toFormattedDateString() }}</td>
          <td>
              @if($order->status != 'unapproved')
                <form action="/{{ Request::segment(1) }}/admin/unapprove-order/{{ $order->id }}" method="POST" style="display:inline!important">
                @csrf
                <button type="submit" onclick="if (!confirm('Are you sure you want to unapprove?')) { return false }" class="btn btn-app" style="background-color: #f39c12; color: white">
                     <i class="fa fa-times"></i> Un-Approve
                </button>
              </form>
              @endif
              @if($order->status != 'approved')
                <form action="/{{ Request::segment(1) }}/admin/approve-order/{{ $order->id }}" method="POST" style="display:inline!important">
                @csrf
                <button type="submit" onclick="if (!confirm('Are you sure you want to approve?')) { return false }" class="btn btn-app" style="background-color: #00a65a; color: white">
                     <i class="fa fa-check"></i> Approve
                </button>
              </form>
              @endif
              <form action="/{{ Request::segment(1) }}/admin/delete-order/{{ $order->id }}" method="POST" style="display:inline!important">
              @csrf
              <button type="submit" onclick="if (!confirm('Are you sure you want to delete?')) { return false }" class="btn btn-app delete" style="background-color: #dd4b39; color: white">
                   <i class="fa fa-trash"></i> Delete
              </button>
            </form>
          </td>
        </tr>
      @endforeach
        </tbody>
      </table>
    </div>
  </div>
@endsection