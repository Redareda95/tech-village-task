@extends('admin.master')
@section('content')
<form method="POST" action="/{{ Request::segment(1) }}/admin/edit-user/{{ $user->id }}"  enctype="multipart/form-data">
    {{ csrf_field() }}
<input type="hidden" name="_method" value="PATCH">
<div class="box box-danger">
    <div class="box-header with-border">
      <h3 class="box-title">Edit User Information</h3>
    </div>
    <div class="form-group row">
      <label class="col-sm-3 col-form-label" for="inputSuccess"><i class="fa fa-user"></i> Name *</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" name="name" id="inputSuccess" value="{{ $user->name }}">
      </div>
      @if ($errors->has('name'))
      <span class="help-block">
          <strong>{{ $errors->first('name') }}</strong>
      </span>
      @endif
    </div>
    <div class="form-group row">
      <label class="col-sm-3 col-form-label" for="inputSuccess"><i class="fa fa-at"></i> Email *</label>
      <div class="col-sm-9">
        <input type="text" class="form-control" name="email" id="inputSuccess" value="{{ $user->email }}">
      </div>
      @if ($errors->has('email'))
      <span class="help-block">
          <strong>{{ $errors->first('email') }}</strong>
      </span>
      @endif
    </div>

    <h4>If You need to change password <code>if not needed leave it empty</code></h4>
    <div class="form-group row">
      <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Password</label>
      <div class="col-sm-9">
        <input type="password" class="form-control" id="exampleInputPassword2" placeholder="Password" name="password">
      </div>
      @if ($errors->has('password'))
      <span class="help-block">
          <strong>{{ $errors->first('password') }}</strong>
      </span>
      @endif
    </div>
    <div class="form-group row">
      <label for="exampleInputPassword2" class="col-sm-3 col-form-label">Confirm Password</label>
      <div class="col-sm-9">
        <input type="password" class="form-control" id="exampleInputPassword2" placeholder="Confirm Password" name="password_confirmation">
      </div>
      @if ($errors->has('password_confirmation'))
      <span class="help-block">
          <strong>{{ $errors->first('password_confirmation') }}</strong>
      </span>
      @endif
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="box-footer">
          <button type="submit" class="btn btn-success btn-block btn-flat">Save</button>
        </div>
      </div>
    </div>
</form>
@endsection
