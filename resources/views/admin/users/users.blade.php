@extends('admin.master')
@section('content')
@if($flash = session('message'))
<div class="alert success">
  <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
    <b>{{ $flash }}</b>
</div>
@endif
@if($flash = session('deleted'))
<div class="alert">
  <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
    <b>{{ $flash }}</b>
</div>
@endif

<div class="box">
<div class="box-header">
  <h3 class="box-title">Data Table For Registered Users</h3>
</div>
    <div class="box-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
          <th>ID</th>
          <th>Name</th>
          <th>Email</th>
          <th>Created At</th>
          <th>Last Login</th>
          <th>Options</th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
        <tr>
          <td>{{ $user->id }}</td>
          <td>{{ $user->name }}</td>
          <td>{{ $user->email }}</td>
          <td>{{ $user->created_at->toFormattedDateString() }}</td>
          <td>{{ \Carbon\Carbon::createFromTimeStamp(strtotime($user->last_login))->diffForHumans() }}</td>
          <td>
            <a href="/{{ Request::segment(1) }}/admin/edit-user/{{ $user->id }}" class="btn btn-app">
                 <i class="fa fa-edit"></i> Edit
            </a>
            <form action="/{{ Request::segment(1) }}/admin/delete-user/{{ $user->id }}" method="POST" style="display:inline!important">
              @csrf
              <button type="submit" onclick="if (!confirm('Are you sure you want to delete?')) { return false }" class="btn btn-app delete" style="background-color: #dd4b39; color: white">
                   <i class="fa fa-trash"></i> Delete
              </button>
            </form>
          </td>
        </tr>
        @endforeach
        </tbody>
      </table>
    </div>
  </div>
@endsection