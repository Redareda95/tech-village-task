@extends('website.master')
@section('content')
<form method="POST" action="/{{ Request::segment(1) }}/edit-product/{{ $product->id }}"  enctype="multipart/form-data">
@csrf
<input type="hidden" name="_method" value="PATCH">

<div class="box box-danger">
  <center class="box-header with-border">
    <h3 class="box-title">{{ trans('main.editp') }}</h3>
  </center>
  <div class="container">
    <div class="row">
      <div class="col-md-6">
      <label for="exampleInputPAN6" class="col-sm-5 col-form-label">{{ trans('main.price') }} *</label>

        <input type="number" class="form-control" id="exampleInputPAN6" value="{{ $product->price }}" name="price">

            @if ($errors->has('price'))
            <span class="help-block">
                <strong>{{ $errors->first('price') }}</strong>
            </span>
            @endif
      </div>
      <div class="col-md-6">
        <div class="form-group">
            <label>{{ trans('main.upload') }} <code>{{ trans('main.upload_txt') }}</code></label>
            <input type="file" name="image" class="form-control" id="imgInp">
            <h5>{{ trans('main.oldi') }}</h5>
            <img id="blah" src="/uploads/{{ $product->image }}" class="img-responsive" width="300" />

              @if ($errors->has('image'))
              <span class="help-block">
                <strong>{{ $errors->first('image') }}</strong>
              </span>
              @endif
        </div>
      </div>
    </div>

    <div class="row">
      @foreach (config('translatable.locales') as $lang => $language)

      <div class="col-md-12">
        <div class="form-group">
           <label for="exampleInputEmail1">{{ trans('main.title') }} * <code>{{ trans('main.in') }} {{ $language }}</code>
          <input type="text" class="form-control" id="exampleInputEmail1" name="title:{{ $lang }}" value="{{ $product->translate($lang)->title }}">
           @if ($errors->has("{{ 'title:' .  $lang }}"))
            <span class="help-block">
                <strong>{{ $errors->first('title: . $lang') }}</strong>
            </span>
            @endif
        </div>

        <hr>
        <div class="box box-danger">
          <div class="box-header">
            <h3 class="box-title">{{ trans('main.cont') }} * <code>{{ trans('main.in') }} {{ $language }}</code>
          </div>
          <div class="box-body pad">
            <textarea id="editor_{{ $lang }}" name="content:{{ $lang }}" rows="10" cols="80">{!! $product->translate($lang)->content !!}</textarea>
            @if ($errors->has("{{ 'content:' . $lang }}"))
            <span class="help-block">
                <strong>{{ $errors->first('content: . $lang') }}</strong>
            </span>
            @endif
          </div>
        </div>
      </div>
      @endforeach
    </div>
  </div>
  <div class="box-footer" style="padding-bottom: 50px">
    <button type="submit" class="btn btn-success btn-block btn-flat">{{ trans('main.save') }}</button>
  </div>
</form>
@endsection