@extends('website.master')
@section('content')
<form method="POST" action="/{{ Request::segment(1) }}/add-product"  enctype="multipart/form-data">
        @csrf
        <div class="box box-danger">
          <center class="box-header with-border">
            <h3 class="box-title">{{ trans('main.add') }}</h3>
          </center>
          <div class="container">
            <div class="row">
              <div class="col-md-6">
              <label for="exampleInputPAN6" class="col-sm-5 col-form-label">Price *</label>

                <input type="number" class="form-control" id="exampleInputPAN6" placeholder="{{ trans('main.price') }}" name="price">

                    @if ($errors->has('price'))
                    <span class="help-block">
                        <strong>{{ $errors->first('price') }}</strong>
                    </span>
                    @endif
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>{{ trans('main.upload') }} <code>{{ trans('main.upload_txt') }}</code> *</label>
                      <input type="file" name="image" class="form-control"  id="imgInp">
                      <img id="blah" src="/addimage.gif" alt="your image" width="150" class=" img-responsive" />
                      @if ($errors->has('image'))
                      <span class="help-block">
                        <strong>{{ $errors->first('image') }}</strong>
                      </span>
                      @endif
                </div>
              </div>
            </div>

            <div class="row">
              @foreach (config('translatable.locales') as $lang => $language)

              <div class="col-md-12">
                <div class="form-group">
                  <label for="exampleInputEmail1">{{ trans('main.title') }} * <code>{{ trans('main.in') }} {{ $language }}</code>
                  </label>
                  <input type="text" class="form-control" id="exampleInputEmail1" placeholder="{{ trans('main.title') }} {{ trans('main.in') }} {{ $language }}" name="title:{{ $lang }}">
                   @if ($errors->has("{{ 'title:' .  $lang }}"))
                    <span class="help-block">
                        <strong>{{ $errors->first('title: . $lang') }}</strong>
                    </span>
                    @endif
                </div>

                <hr>
                <div class="box box-danger">
                  <div class="box-header">
                    <h3 class="box-title">{{ trans('main.cont') }} * <code>{{ trans('main.in') }} {{ $language }}</code>
                    </h3>
                  </div>
                  <div class="box-body pad">
                    <textarea name="content:{{ $lang }}" rows="10" cols="80"></textarea>
                    @if ($errors->has("{{ 'content:' . $lang }}"))
                    <span class="help-block">
                        <strong>{{ $errors->first('content: . $lang') }}</strong>
                    </span>
                    @endif
                  </div>
                </div>
              </div>
              @endforeach
            </div>
          </div>
        <div class="box-footer" style="padding-bottom: 50px">
            <center>
                <button type="submit" class="btn btn-primary btn-flat">
                    {{ trans('main.save') }}
                </button>
            </center>
        </div>
        </form>
@endsection