@extends('website.master')
@section('content')
<div class="container" style="padding: 150px">
	<div class="row">
       <div class="col-xs-4 item-photo">
            <img style="max-width:100%;" src="/uploads/{{ $product->image }}" />
        </div>
        <div class="col-xs-5" style="border:0px solid gray">
            <!-- Datos del vendedor y titulo del producto -->
            <h3>{{ $product->title }}</h3>    
            <!-- Precios -->
            <h6 class="title-price"><small>{{ trans('main.price') }}</small></h6>
            <h3 style="margin-top:0px;">{{ $product->price }}</h3>
            <p>
             {!! $product->content !!}
        	</p>    

            <!-- Botones de compra -->
            <div class="section" style="padding-bottom:20px;">
                <a href="{{ route('product.order', $product->slug) }}">
                    <button class="btn btn-success">
                    	<span style="margin-right:20px" class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span>
                    	{{ trans('main.order') }}
                    </button>
                </a>
            </div>                                        
        </div>  		
    </div>
</div>        
@endsection