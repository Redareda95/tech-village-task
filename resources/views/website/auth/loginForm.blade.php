@extends('website.master')
@section('content')

<div class="container" style="padding: 112px;">
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<div class="panel panel-login">
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-6">
							<a href="#" class="active" id="login-form-link">{{ trans('login.head') }}</a>
						</div>
						<div class="col-xs-6">
							<a href="#" id="register-form-link">{{ trans('login.head3') }}</a>
						</div>
					</div>
					<hr>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-lg-12">
							<form id="login-form" action="/{{ Request::segment(1) }}/login" method="post" role="form" style="display: block;">
								@csrf
								<div class="form-group">
									<input type="text" name="email" id="username" tabindex="1" class="form-control" placeholder="{{ trans('login.userl') }}" value="">
									@if ($errors->has('email'))
				                    <span class="help-block">
				                        <strong>{{ $errors->first('email') }}</strong>
				                    </span>
				                    @endif
				                    @if ($errors->has('message'))
				                    <span class="help-block">
				                        <strong>{{ $errors->first('message') }}</strong>
				                    </span>
				                    @endif
								</div>
								<div class="form-group">
									<input type="password" name="password" id="password" tabindex="2" class="form-control" placeholder="{{ trans('login.passl') }}">
									@if ($errors->has('password'))
				                    <span class="help-block">
				                        <strong>{{ $errors->first('password') }}</strong>
				                    </span>
				                    @endif
								</div>
								<div class="form-group text-center">
									<input type="checkbox" tabindex="3" class="" name="remember" id="remember">
									<label for="remember"> {{ trans('login.keep') }}</label>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-sm-6 col-sm-offset-3">
											<input type="submit" name="login-submit" id="login-submit" tabindex="4" class="form-control btn btn-login" value="{{ trans('login.login') }}">
										</div>
									</div>
								</div>
							</form>
							<form id="register-form" action="/{{ Request::segment(1) }}/register" method="post" role="form" style="display: none;">
								@csrf
								<div class="form-group">
									<input type="text" name="name" id="username" tabindex="1" class="form-control" placeholder="{{ trans('login.name') }}" value="">
									@if ($errors->has('name'))
			                            <span class="help-block">
			                                <strong>{{ $errors->first('name') }}</strong>
			                            </span>
			                         @endif
								</div>
								<div class="form-group">
									<input type="email" name="email" id="email" tabindex="1" class="form-control" placeholder="{{ trans('login.email') }}" value="">
									@if ($errors->has('email'))
			                            <span class="help-block">
			                                <strong>{{ $errors->first('email') }}</strong>
			                            </span>
			                         @endif
								</div>
								<div class="form-group">
									<input type="password" name="password" id="password" tabindex="2" class="form-control" placeholder="{{ trans('login.passl') }}">
									@if ($errors->has('password'))
			                            <span class="help-block">
			                                <strong>{{ $errors->first('password') }}</strong>
			                            </span>
			                         @endif
								</div>
								<div class="form-group">
									<input type="password" name="password_confirmation" id="confirm-password" tabindex="2" class="form-control" placeholder="{{ trans('login.pass2') }}">
									@if ($errors->has('password_confirmation'))
			                            <span class="help-block">
			                                <strong>{{ $errors->first('password_confirmation') }}</strong>
			                            </span>
			                         @endif
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-sm-6 col-sm-offset-3">
											<input type="submit" name="register-submit" id="register-submit" tabindex="4" class="form-control btn btn-register" value="{{ trans('login.regBtn') }}">
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection