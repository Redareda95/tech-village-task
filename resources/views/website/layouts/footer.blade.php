<!--Bottom Footer-->
<div class="bottom section-padding">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<div class="copyright">
					<p>© <span>2019</span> <a href="#" class="transition">AhmedReda</a> {{ trans('main.footer') }}.</p>
				</div>
			</div>
		</div>
	</div>
</div>
<!--Bottom Footer-->
<script src="/js/login.js"></script>
@if($flash = session('message'))
<script>
    alert('{{ $flash }}');
</script>
@endif
@if($flash = session('deleted'))
<script>
    alert('{{ $flash }}');
</script>
@endif
<script type="text/javascript">
  function readURL(input) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      $('#blah').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
  }
}

$("#imgInp").change(function() {
  readURL(this);
});
</script>
</body>
</html>