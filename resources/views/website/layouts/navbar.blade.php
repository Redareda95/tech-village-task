<!DOCTYPE html>
 <html lang="en">
 <head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta charset="utf-8" />

<meta name="theme-color" content="#800000">

<title>{{ $seo->title }}</title>

<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<link rel="icon" type="image/x-icon" href="/dist/img/avatar.png" />

<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<link rel="stylesheet" type="text/css" href="/css/style.css">
<link rel="stylesheet" type="text/css" href="/css/login.css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->
</head>
<body>
<div class="container-fluid">
    <!-- Second navbar for profile settings -->
    <nav class="navbar navbar-inverse">
      <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-4">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/{{ Request::segment(1) }}">TechVillage</a>
        </div>
    
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="navbar-collapse-4">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="/{{ Request::segment(1) }}">{{ trans('main.home') }}</a></li>
            <li><a href="/{{ Request::segment(1) }}#products">{{ trans('main.prods') }}</a>
            </li>
             @if(\Auth::check())
            <li>
              <a href="/{{ Request::segment(1) }}/my-products">{{ trans('main.my') }}</a>
            </li>
            @endif 
             @foreach (config('translatable.locales') as $lang => $language)
                @if ($lang != app()->getLocale())
                    <li>
                        <a href="{{ route('lang.switch', $lang) }}">
                            {{ $language }}
                        </a>
                    </li>
                @endif
            @endforeach
            @if(\Auth::check())
            <li>
              <form action="/{{ Request::segment(1) }}/logout" method="POST" style="display:inline!important;">
                @csrf
                <a class="btn btn-default btn-outline btn-circle txt-white"  data-toggle="collapse" href="javascript:;" onclick="parentNode.submit();" aria-expanded="false" aria-controls="nav-collapse4">{{ trans('main.log') }} <i class=""></i> </a>
              </form>
            </li>
            <li>
                <a class="btn btn-default btn-outline btn-circle txt-white"  data-toggle="collapse" href="/{{ Request::segment(1) }}/add-product" aria-expanded="false" aria-controls="nav-collapse4">{{ trans('main.add') }}<i class=""></i> </a>
              
            </li>
            @else
            <li>
                <a class="btn btn-default btn-outline btn-circle txt-white"  data-toggle="collapse" href="/{{ Request::segment(1) }}/login" aria-expanded="false" aria-controls="nav-collapse4">{{ trans('main.login') }} <i class=""></i> </a>
            </li>
            @endif
          </ul>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container -->
    </nav><!-- /.navbar -->
</div><!-- /.container-fluid -->