@extends('website.master')
@section('content')
<!-- page-header -->
<div class="page-header">
    <div class="container">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-caption">
                    <h1 class="page-title">TechVillage Task</h1>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.page-header-->
<!-- news -->

@if(!$products->isEmpty())
<div class="container products" id="products">
    <div class="row">
        <div class="col-md-12">
            <h1>
            	<center>
                   {{ trans('main.all') }}
				</center>
           	 </h1>
        </div>
    </div>

	@foreach($products->chunk(2) as $products_chunked)
	  <div class="row">
	    @foreach($products_chunked as $product)
	    <div class="col-md-6">
			<div class="thumbnail">
				<a href="{{ route('product.show', $product->slug) }}">
					<img src="/uploads/{{ $product->image }}" alt="{{ $product->title }}"/></a>
			</div>
			<ul class="list-inline">
	            <li>{{ $product->title }}</li>
	            <li class='pull-right'>{{ $product->price }} {{ trans('main.curr') }}</li>
	        </ul>
	        <a href="{{ route('product.order', $product->slug) }}" class="btn btn-info col-md-6">{{ trans('main.order') }}</a>
	        <a href="{{ route('product.show', $product->slug) }}" class="btn btn-warning col-md-6">{{ trans('main.show') }}</a>
	    </div>
	    @endforeach
    </div>
    @endforeach
    <!-- Start Pagination -->
     <div class="col-md-12"> 
       {{ $products->render() }}
     </div>
     <!-- End Pagination -->
</div>
@endif
@endsection