<?php
return [

	//navbar.blade.php
	'home' => 'Home',
	'prods' => 'Products',
	'log' => 'Logout',
	'add' => 'Add Product',
	'login' => 'Login / Register',
	//footer.blade.php
	'footer' => 'All rights reserved',
	//others
	'all' => 'All Products',
	'curr' => 'L.E',
	'order' => 'Order Now',
	'show' => 'Show',
	'price' => 'Price',
	'my' => 'My Products',
	'edit' => 'Edit',
	'delete' => 'Delete',
	'sure' => 'Are You  sure you want to delete',
	'upload' => 'Upload Image',
	'upload_txt' => 'Maximum Size 2MB',
	'title' => 'Product Title',
	'cont' => 'Product Description',
	'in' => 'In',
	'save' => 'Save',
	'editp' => 'Edit Product',
	'oldi' => 'Old Photo',

];
?>