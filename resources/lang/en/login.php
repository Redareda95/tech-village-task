<?php
return [

	'head' => 'Login',
	'userl' => 'Email',
	'passl' => 'Password',
	'keep' => 'Keep me logged in',
	'login' => 'Login',
	'head3' => 'Register',
	'name' => 'Name', 
	'email' => 'Email', 
	'pass2' => 'Confirm Password',
	'regBtn' => 'Register Now'

];
?>