<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;
use App\User;

class Product extends Model
{
    use Translatable;

	protected $fillable = ['price', 'image'];

    public $translatedAttributes = ['title', 'slug', 'content'];

    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
