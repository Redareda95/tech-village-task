<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\User;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::get();

        return view('admin.products.products', compact('products'));
    }

    public function create()
    {
    	$users = User::get();

        return view('admin.products.add', compact('users'));
    }

    public function store(Request $request)
    {
    	$this->validate(request(),[
            'image' => 'required|image|mimes:jpg,jpeg,png,gif|max:2048',
            'user_id' => 'required',
            'price' => 'required|numeric|digits_between:1,4',
        ]);

    	foreach (config('translatable.locales') as $l => $lan) 
		{
			$title = 'title:' . $l;

			$content = 'content:' . $l;

	    	$this->validate(request(),[
	            $title => 'required|max:191',
	            $content => 'required',
	        ]);
    	}

    	$img_name = time() . '.' . $request->image->getClientOriginalExtension();

		$product = new Product;

        $product->image = $img_name;

		$product->user_id = $request->user_id;

		$product->price = $request->price;

		$product->save();

        $request->image->move('uploads', $img_name);

		foreach (config('translatable.locales') as $lang => $locale) 
		{
	    	$product->translateOrNew($lang)->title = $request->input('title:' . $lang);
	    
	    	$product->translateOrNew($lang)->content = $request->input('content:' . $lang);
	    }
	 
	    $product->save();		

	    session()->flash('message', 'Product Is Created');

        return redirect('/' . \Request::segment(1) . '/admin/products');
    }

    public function edit($id)
    {
        $product = Product::findOrFail($id);

        $users = User::get();

        return view('admin.products.edit', compact('product', 'users'));
    }

    public function update(Request $request, $id)
    {
        $this->validate(request(),[
            'image' => 'image|mimes:jpg,jpeg,png,gif|max:2048',
            'user_id' => 'required',
            'price' => 'required|numeric|digits_between:1,4',
        ]);

        foreach (config('translatable.locales') as $l => $lan) 
        {
            $title = 'title:' . $l;

            $content = 'content:' . $l;

            $this->validate(request(),[
                $title => 'required|max:191',
                $content => 'required',
            ]);
        }

        $product = Product::findOrFail($id);

        if(file_exists($request->file('image')))
        {
           $img_name = time() . '.' . $request->image->getClientOriginalExtension();

           $product->update(['image' => $img_name]);
                     
           $request->image->move('uploads', $img_name);

        }

        $product->update([
                          'price' => $request->price,
                          'user_id' => $request->user_id
                         ]);

        foreach (config('translatable.locales') as $lang => $locale) 
        {
            $product->translate($lang)->title = $request->input('title:' . $lang);
        
            $product->translate($lang)->content = $request->input('content:' . $lang);
        }

        $product->save();

        session()->flash('message', 'Product Is Updated');

        return redirect('/' . \Request::segment(1) . '/admin/products');
    }

    public function destroy($id)
    {
        $product = Product::findOrFail($id);

        $product->delete();

        session()->flash('deleted', 'Product is Deleted');

        return back();
    }
}
