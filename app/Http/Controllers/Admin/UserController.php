<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class UserController extends Controller
{
    public function index()
    {
        $users = User::get();

        return view('admin.users.users', compact('users'));
    }

    public function create()
    {
        return view('admin.users.add');
    }

    public function store(Request $request)
    {
        $this->validate(request(),[
            'name' => 'required|max:191',
            'email'=>'required|email|unique:admins',
            'password'=>'required|string|min:6|confirmed'
        ]);

        $pass = $request->password;

        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($pass);
        $user->last_login = now();
        $user->save();

        session()->flash('message', 'User Is Created');

        return redirect('/' . \Request::segment(1) . '/admin/users');
    }

    public function edit($id)
    {
        $user = User::findOrFail($id);

        return view('admin.users.edit', compact('user'));
    }

     public function update(Request $request, $id) 
    {
        $this->validate(request(),[
            'name' => 'required|max:191',
            'email'=>'required|email|unique:admins,email,'.$id,
        ]);

        User::where('id', $id)->update([
                                        'name' => $request->name, 
                                        'email' => $request->email,
                                    ]);

        if (isset($request->password)) 
        {
            $this->validate(request(),[
                'password' => 'required|min:6|confirmed',
            ]);

            $password = request(['password']);
            
            User::where('id', $id)->update(['password' => bcrypt($password['password'])]);

        }

        session()->flash('message', 'User Information Is Updated');

        return redirect('/' . \Request::segment(1) . '/admin/users');

    }


    public function destroy($id)
    {
        $user = User::findOrFail($id);

        $user->delete();

        session()->flash('deleted', 'User is Deleted');

        return back();
    }
}
