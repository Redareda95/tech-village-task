<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;

class OrderController extends Controller
{
    public function index()
    {
        $orders = Order::get();

        return view('admin.orders.orders', compact('orders'));
    }

    public function destroy($id)
    {
        $order = Order::findOrFail($id);

        $order->delete();

        session()->flash('deleted', 'Order is Deleted');

        return back();
    }

    public function approve($id)
    {
    	$order = Order::findOrFail($id);

    	if($order->status == 'approved')
    	{	
	        session()->flash('deleted', 'Order is already approved');

    		return back();
    	}

    	$order->update([
						  'status' => 'approved'
    				  ]);

    	$order->save();

    	session()->flash('message', 'Order is approved');

		return back();

    }

    public function unapprove($id)
    {
    	$order = Order::findOrFail($id);

    	if($order->status == 'unapproved')
    	{	
	        session()->flash('deleted', 'Order is already unadpprove');

    		return back();
    	}

    	$order->update([
						  'status' => 'unapproved'
    				  ]);

    	$order->save();

    	session()->flash('message', 'Order is Un-approved');

		return back();

    }
}
