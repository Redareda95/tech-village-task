<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admin;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    public function create()
    {
        return view('admin.admins.add');
    }

    public function store(Request $request)
    {
        $this->validate(request(),[
            'name' => 'required|max:191',
            'email'=>'required|email|unique:admins',
            'password'=>'required|string|min:6|confirmed'
        ]);

        $pass = $request->password;

        $admin = new Admin;
        $admin->name = $request->name;
        $admin->email = $request->email;
        $admin->password = Hash::make($pass);
        $admin->last_login = now();
        $admin->save();

        session()->flash('message', 'Admin Is Created');

        return redirect('/' . \Request::segment(1) . '/admin/admins');
    }

    public function edit($id)
    {
        $user = Admin::findOrFail($id);

        return view('admin.admins.edit', compact('user'));
    }

     public function update(Request $request, $id) 
    {
        $this->validate(request(),[
            'name' => 'required|max:191',
            'email'=>'required|email|unique:admins,email,'.$id,
        ]);

        Admin::where('id', $id)->update([
                                        'name' => $request->name, 
                                        'email' => $request->email,
                                    ]);

        if (isset($request->password)) 
        {
            $this->validate(request(),[
                'password' => 'required|min:6|confirmed',
            ]);

            $password = request(['password']);
            
            Admin::where('id', $id)->update(['password' => bcrypt($password['password'])]);

        }

        session()->flash('message', 'Admin Information Is Updated');

        return redirect('/' . \Request::segment(1) . '/admin/admins');

    }

    public function index()
    {
        $admins = Admin::get();

        return view('admin.admins.admins', compact('admins'));
    }

    public function destroy($id)
    {
        $admin = Admin::findOrFail($id);

        if($admin->id == auth()->guard('admin')->user()->id)
        {
            session()->flash('deleted', 'Can\'t Delete Current Admin');

            return back();
        }

        $admin->delete();

        session()->flash('deleted', 'Admin is Deleted');

        return back();
    }
}
