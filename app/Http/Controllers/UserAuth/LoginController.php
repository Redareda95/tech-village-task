<?php

namespace App\Http\Controllers\UserAuth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class LoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest')->except(['logout']);;
    }

    public function loginView (Request $request)
    {
        $seo = (object) collect([
                        'title' => 'Login / Register :: TechVillage Task',
                ])->all();

    	return view('website.auth.loginForm', compact('seo'));
    }

    public function login()
    {
    	if(! auth()->attempt(request(['email','password'])))
    	{
    		return back()->withErrors(['message'=>'Not correct informations']);
    	}

    	$id = \Auth::user()->id;

        User::where('id', $id)->update(['last_login'=>now()]);

        session()->flash('message', 'Hello ' . \Auth::user()->name . ' ! you have Login Successfully...');

    	return redirect('/');
    }


    public function logout ()
    {
    	auth()->logout();

        session()->flash('message', 'Success Logout...');

    	return redirect('/');
    }
}
