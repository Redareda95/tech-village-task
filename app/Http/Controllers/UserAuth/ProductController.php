<?php

namespace App\Http\Controllers\UserAuth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;

class ProductController extends Controller
{
    public function index()
    {
    	$id = auth()->user()->id;

    	$products = Product::where('user_id', $id)->paginate(6);

    	$seo = (object) collect([
                        'title' => auth()->user()->name . ' :: TeechVillage Task',
                ])->all();

    	return view('website.products.products', compact('products', 'seo'));
    }

    public function create()
    {
        $seo = (object) collect([
                        'title' => 'Create Post :: TeechVillage Task',
                ])->all();

    	return view('website.products.add', compact('seo'));
    }

    public function store(Request $request)
    {
    	$this->validate(request(),[
            'image' => 'required|image|mimes:jpg,jpeg,png,gif|max:2048',
            'price' => 'required|numeric|digits_between:1,4',
        ]);

    	foreach (config('translatable.locales') as $l => $lan) 
		{
			$title = 'title:' . $l;

			$content = 'content:' . $l;

	    	$this->validate(request(),[
	            $title => 'required|max:191',
	            $content => 'required',
	        ]);
    	}

    	$id = auth()->user()->id;

    	$img_name = time() . '.' . $request->image->getClientOriginalExtension();

		$product = new Product;

        $product->image = $img_name;

		$product->user_id = $id;

		$product->price = $request->price;

		$product->save();

        $request->image->move('uploads', $img_name);

		foreach (config('translatable.locales') as $lang => $locale) 
		{
	    	$product->translateOrNew($lang)->title = $request->input('title:' . $lang);
	    
	    	$product->translateOrNew($lang)->content = $request->input('content:' . $lang);
	    }
	 
	    $product->save();		

	    session()->flash('message', 'Product Is Created');

        return redirect('/' . \Request::segment(1) . '/my-products');
    }

    public function edit($id)
    {
        $product = Product::findOrFail($id);

        $seo = (object) collect([
                        'title' => 'Edit Products :: TeechVillage Task',
                ])->all();

        $id = auth()->user()->id;

        if($id != $product->user_id)
        {
        	return back();
        }

        return view('website.products.edit', compact('product', 'seo'));
    }

    public function update(Request $request, $id)
    {
        $this->validate(request(),[
            'image' => 'image|mimes:jpg,jpeg,png,gif|max:2048',
            'price' => 'required|numeric|digits_between:1,4',
        ]);

        foreach (config('translatable.locales') as $l => $lan) 
        {
            $title = 'title:' . $l;

            $content = 'content:' . $l;

            $this->validate(request(),[
                $title => 'required|max:191',
                $content => 'required',
            ]);
        }

        $product = Product::findOrFail($id);

        if(file_exists($request->file('image')))
        {
           $img_name = time() . '.' . $request->image->getClientOriginalExtension();

           $product->update(['image' => $img_name]);
                     
           $request->image->move('uploads', $img_name);

        }

        $product->update([
                          'price' => $request->price,
                         ]);

        foreach (config('translatable.locales') as $lang => $locale) 
        {
            $product->translate($lang)->title = $request->input('title:' . $lang);
        
            $product->translate($lang)->content = $request->input('content:' . $lang);
        }

        $product->save();

        session()->flash('message', 'Product Is Updated');

        return redirect('/' . \Request::segment(1) . '/my-products');
    }

    public function destroy($id)
    {
        $product = Product::findOrFail($id);

        $id = auth()->user()->id;

        if($id != $product->user_id)
        {
        	return back();
        }

        $product->delete();

        session()->flash('deleted', 'Product is Deleted');

        return back();
    }
}
