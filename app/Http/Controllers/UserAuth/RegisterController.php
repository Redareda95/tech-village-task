<?php

namespace App\Http\Controllers\UserAuth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class RegisterController extends Controller
{
     public function __construct ()
    {
       $this->middleware('guest');
    }

    public function show ()
    {
        $seo = (object) collect([
                'title' => 'Login\Register :: TechVillage Task',
                ])->all();

		return view('website.auth.loginForm', compact('seo'));    
	}

	public function register (Request $request)
    {
    	$this->validate(request(),[
            'name'=>'required|min:5|max:25',
            'email'=>'required|email',
            'password'=>'required|confirmed'
        ]);

    	$pass = request(['password']);

        $user= new User;

        $user->name = request('name');

        $user->email = request('email');

        $user->password = bcrypt($pass['password']);

        $user->last_login = now();

        $user->save();

        auth()->login($user);

        session()->flash('message', 'Congratulates! you have Login Successfully...');

    	return redirect('/');
    }

}
