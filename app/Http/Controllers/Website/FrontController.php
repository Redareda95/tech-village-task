<?php

namespace App\Http\Controllers\Website;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;

class FrontController extends Controller
{
    public function index()
    {
    	$products = Product::paginate(6);

    	$seo = (object) collect([
                        'title' => 'Homepage :: TechVillage Task',
                ])->all();

        return view('website.welcome', compact('products', 'seo'));
    }
}
