<?php

namespace App\Http\Controllers\Website;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\Order;

class SingleController extends Controller
{
    public function show($slug)
    {
        $product = Product::whereTranslation('slug', $slug)->firstOrFail();

        // New Code
        if ($product->translate()->where('slug', $slug)->first()->locale != app()->getLocale()) {
            return redirect()->route('product.show', $product->translate()->slug);
        }

        $seo = (object) collect([
                        'title' => $product->title . ' :: TechVillage Task',
                ])->all();

        return view('website.single_product', compact('seo', 'product'));
    }

    public function order($slug)
    {
        if(!\Auth::check())
        {
            return redirect('/' . \Request::segment(1) . '/login');
        }

        $product = Product::whereTranslation('slug', $slug)->firstOrFail();

        $user_id = auth()->user()->id;

        Order::create([
                        'user_id' => $user_id,
                        'product_id' => $product->id
                     ]);

        session()->flash('message', 'Order made successfully');

        return back();

    }
}
